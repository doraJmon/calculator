#include "calculator_io.h"
// #include <QDebug>

bool calculator_io::isOperator(QString s) {
  return s == '+' || s == '-' || s == '*' || s == '/';
}

int calculator_io::getPriority(QString s) {
  if (s == '(') {
    return 0;
  } else if (s == '+' || s == '-') {
    return 1;
  } else {
    return 2;
  }
}

QQueue <QString> calculator_io::split(QString exp) {
  QQueue <QString> res;
  QString num = "";
  auto parentCount = 0;

  // Make sure the 1st position (exp[0]) is legal
  if (exp[0] == '*' || exp[0] == '/' || exp[0] == ')') {
    throw "Error";
  } else if (exp[0] == '(') {
    res.enqueue(QString('('));
    parentCount++;
  } else if (exp[0] == '.') {
    num += "0.";
  } else {
    num += exp[0];
  }

  // Make sure the last position (exp[len-1]) is legal
  if ((exp[exp.length()-1] != ')') &&
      (!exp[exp.length()-1].isDigit()))  throw "Error";

  // Make sure all the positions are legal
  for (int i = 1; i < exp.length(); ++i) {
    auto nowChar = char(exp[i].unicode());
    switch (nowChar) {
      case '+': case '-': case '*': case '/': {
        if ((!exp[i-1].isDigit()) &&
            (exp[i-1] != '(' && exp[i-1] != ')'))  throw "Error";
        break;
      } case '.': {
        if (exp[i-1] == ')' || exp[i-1] == '.')  throw "Error";
        break;
      } case '(': {
        if (exp[i-1].isDigit() || exp[i-1] == '.')  throw "Error";
        parentCount++;
        break;
      } case ')': {
        if (!exp[i-1].isDigit())  throw "Error";
        parentCount--;
        break;
      } default: {
        if (exp[i-1] == ')')  throw "Error";
        break;
      }
    }
    if (parentCount < 0)  throw "Error";

    // Split elements
    if (exp[i].isDigit() || exp[i] == '.') {
      num += exp[i];
    } else {
      if (!num.isEmpty()) {
        res.enqueue(num);
        num.clear();
      }
      if ((exp[i] == '+' || exp[i] == '-') &&
          (exp[i-1] == '*' || exp[i-1] == '/' || exp[i-1] == '(')) {
        num += exp[i];
      } else {
        res.enqueue(QString(exp[i]));
      }
    }
  }
  if (!num.isEmpty())  res.enqueue(num);
  if (parentCount)  throw "Error";
  return res;
}

QQueue <QString> calculator_io::infixToSuffix(QQueue <QString> infix) {
  // The final return value is the QQueue "suffix"
  QQueue <QString> suffix;
  // Store the default operators, only includd '+', '-', '*', '/', '('
  QStack <QString> oprt;

  while (!infix.isEmpty()) {
    QString ifront = infix.front();
    infix.pop_front();
    if (ifront == ')') {
      while ((!oprt.isEmpty()) &&
             (oprt.top() != '(')) {
        suffix.push_back(oprt.pop());
      }
      oprt.pop();
    } else if (ifront == '(') {
      oprt.push("(");
    } else if (isOperator(ifront)) {
      while ((!oprt.isEmpty()) &&
             (getPriority(oprt.top()) >= getPriority(ifront))) {
        suffix.push_back(oprt.pop());
      }
      oprt.push(ifront);
    } else {
      suffix.push_back(ifront);
    }
  }
  while (!oprt.isEmpty()) {
    suffix.push_back(oprt.pop());
  }
  return suffix;
}

QString calculator_io::getResult(QQueue <QString> suffix) {
  QStack <QString> calc;
  while (!suffix.isEmpty()) {
    QString sfront = suffix.front();
    suffix.pop_front();
    if (isOperator(sfront)) {
      auto second = calc.pop().toDouble();
      auto first = calc.pop().toDouble();
      double tmpres = 0.0;
      if (sfront == '+') {
        tmpres = first + second;
      } else if (sfront == '-') {
        tmpres = first - second;
      } else if (sfront == '*') {
        tmpres = first * second;
      } else {
        tmpres = first / second;
      }
      calc.push(QString::number(tmpres));
    } else {
      calc.push(sfront);
    }
  }
  return calc.pop();
}
