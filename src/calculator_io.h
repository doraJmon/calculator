#ifndef CALCULATOR_IO_H
#define CALCULATOR_IO_H

#include <QQueue>
#include <QStack>
#include <QString>

class calculator_io {
public:
  calculator_io();
  static bool isOperator(QString);
  static int getPriority(QString);
  static QQueue <QString> split(QString);
  static QQueue <QString> infixToSuffix(QQueue <QString>);
  static QString getResult(QQueue <QString>);
};

#endif // CALCULATOR_IO_H
