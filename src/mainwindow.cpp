#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "about.h"
#include "calculator_io.h"
// #include <QDebug>

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow) {
  ui->setupUi(this);
  ui->label_screen->setText("");
}

MainWindow::~MainWindow() {
  delete ui;
}

void MainWindow::on_pushButton_0_clicked() {
  addText("0");
}

void MainWindow::on_pushButton_1_clicked() {
  addText("1");
}

void MainWindow::on_pushButton_2_clicked() {
  addText("2");
}

void MainWindow::on_pushButton_3_clicked() {
  addText("3");
}

void MainWindow::on_pushButton_4_clicked() {
  addText("4");
}

void MainWindow::on_pushButton_5_clicked() {
  addText("5");
}

void MainWindow::on_pushButton_6_clicked() {
  addText("6");
}

void MainWindow::on_pushButton_7_clicked() {
  addText("7");
}

void MainWindow::on_pushButton_8_clicked() {
  addText("8");
}

void MainWindow::on_pushButton_9_clicked() {
  addText("9");
}

void MainWindow::on_pushButton_clear_clicked() {
  ui->label_screen->setText("");
  isResult = false;
}

void MainWindow::on_pushButton_plus_clicked() {
  addText("+");
}

void MainWindow::on_pushButton_minus_clicked() {
  addText("-");
}

void MainWindow::on_pushButton_multiply_clicked() {
  addText("*");
}

void MainWindow::on_pushButton_divise_clicked() {
  addText("/");
}

void MainWindow::on_pushButton_back_clicked() {
  if (isResult) {
    ui->label_screen->setText("");
    isResult = false;
    return;
  }
  QString str = ui->label_screen->text();
  if (str.isEmpty())  return;
  str.chop(1);
  ui->label_screen->setText(str);
}

void MainWindow::on_pushButton_dot_clicked() {
  addText(".");
}

void MainWindow::on_pushButton_lparent_clicked() {
  addText("(");
}

void MainWindow::on_pushButton_rparent_clicked() {
  addText(")");
}

void MainWindow::on_pushButton_equal_clicked() {
  isResult = true;
  try {
    QString expression = ui->label_screen->text();
    if (expression.isEmpty())  return;
    QQueue <QString> expWord = calculator_io::split(expression);
    // qDebug() << "Expression splitted: \n" << expWord << "\n\n";
    QQueue <QString> sufWord = calculator_io::infixToSuffix(expWord);
    // qDebug() << "Expression conversed: \n" << sufWord << "\n\n";
    expression = calculator_io::getResult(sufWord);
    ui->label_screen->setText(expression);
  } catch(...) {
    ui->label_screen->setText("Error");
  }
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_actionAbout_triggered()
{
    about *a = new about;
    a->setWindowTitle("About");
    a->show();
}

void MainWindow::addText(QString s) {
  if (isResult) {
    ui->label_screen->setText(s);
    isResult = false;
  } else {
    QString disp = ui->label_screen->text();
    ui->label_screen->setText(disp + s);
  }
}
