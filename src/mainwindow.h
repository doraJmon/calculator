#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_pushButton_0_clicked();
  void on_pushButton_1_clicked();
  void on_pushButton_2_clicked();
  void on_pushButton_3_clicked();
  void on_pushButton_4_clicked();
  void on_pushButton_5_clicked();
  void on_pushButton_6_clicked();
  void on_pushButton_7_clicked();
  void on_pushButton_8_clicked();
  void on_pushButton_9_clicked();
  void on_pushButton_clear_clicked();
  void on_pushButton_plus_clicked();
  void on_pushButton_minus_clicked();
  void on_pushButton_multiply_clicked();
  void on_pushButton_divise_clicked();
  void on_pushButton_back_clicked();
  void on_pushButton_dot_clicked();
  void on_pushButton_lparent_clicked();
  void on_pushButton_rparent_clicked();
  void on_pushButton_equal_clicked();

  void on_actionQuit_triggered();

  void on_actionAbout_triggered();

private:
  Ui::MainWindow *ui;
  bool isResult = false;
  void addText(QString);
};

#endif // MAINWINDOW_H
